﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;

namespace TestCase
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<TableNames> tableNames = new List<TableNames>
            {
                new TableNames
                {
                    internalId = null,
                    regulator = null,
                    sphere = null,
                    resultActNumber = null,
                    status = null,
                    risk = null,
                    isPlanned = null,
                    sanction = null,
                    date = null,
                    link = null
                }
            };
            string pathCsvFile = @"C:\Users\User\source\repos\TestCase\TestCase.csv";
            WebRequest request = WebRequest.Create("http://api.ias.brdo.com.ua/v1_1/inspections?apiKey=360d858edaac8313a73d237f340138c097ab6304&year=2019");
            WebResponse response = request.GetResponse();
            using (StreamReader stream = new StreamReader(response.GetResponseStream()))
            {
                string data1;
                if ((data1 = stream.ReadLine()) != null)
                {
                    int flag = 0;
                    JsonDocument document = JsonDocument.Parse(data1);
                    JsonElement root = document.RootElement;
                    JsonElement itemsElement = root.GetProperty("items");
                    int count = itemsElement.GetArrayLength();
                    foreach (JsonElement item in itemsElement.EnumerateArray())
                    {
                        item.TryGetProperty("internal_id", out JsonElement internalidElement); //ідентифікаційний код (1)
                        item.TryGetProperty("regulator", out JsonElement regulatorElement);    //контролюючий орган (2)
                        JsonElement dataElement = item.GetProperty("data");
                        dataElement.TryGetProperty("sphere", out JsonElement sphereElement);   //сфера контролю (3)
                        dataElement.TryGetProperty("result_act_number", out JsonElement resultactnumberElement); //перевірка № (4)
                        dataElement.TryGetProperty("status", out JsonElement statusElement);   //статус перевірки (5)
                        dataElement.TryGetProperty("risk", out JsonElement riskElement);       //ступінь ризику (6)
                        item.TryGetProperty("is_planned", out JsonElement isplannedElement);   //тип перевірки (7)
                        JsonElement sanctionFineAmountElement;
                        item.TryGetProperty("sanction", out sanctionFineAmountElement);
                        JsonElement sanctionsElement = dataElement.GetProperty("sanction");
                        if (sanctionsElement.GetArrayLength() != 0)
                        {
                            foreach (JsonElement sanctionItem in sanctionsElement.EnumerateArray())
                            {
                                JsonElement sanctionRegulatorElement = sanctionItem.GetProperty("regulator");
                                sanctionRegulatorElement.TryGetProperty("sanction_fine_amount", out sanctionFineAmountElement);
                            }                            
                        }                        
                        dataElement.TryGetProperty("date_start", out JsonElement datestartElement); //дати проведення (9.1)
                        dataElement.TryGetProperty("date_finish", out JsonElement datefinishElement); //дати проведення (9.2)
                        string datefinishcorrect;
                        if (datefinishElement.ToString() == "False")
                        {
                            datefinishcorrect = "Не вказано";
                        }
                        else
                        {
                            datefinishcorrect = datefinishElement.ToString();
                        }
                        dataElement.TryGetProperty("link", out JsonElement linkElement); //посилання на картку з результатами (10)


                        TableNames tn = new TableNames
                        {
                            internalId = internalidElement.ToString(),
                            regulator = regulatorElement.ToString(),
                            sphere = isplannedElement.ToString(),
                            resultActNumber = resultactnumberElement.ToString(),
                            status = statusElement.ToString(),
                            risk = riskElement.ToString(),
                            isPlanned = isplannedElement.ToString(),
                            sanction = sanctionFineAmountElement.ToString(),
                            date = datestartElement.ToString() + " - " + datefinishcorrect,
                            link = linkElement.ToString()
                        };
                        
                        tableNames.Insert(flag, tn);
                        flag += 1;
                    }
                    using (StreamWriter streamReader = new StreamWriter(pathCsvFile, true, Encoding.UTF8))
                    {
                        using (CsvWriter csvReader = new CsvWriter(streamReader, System.Globalization.CultureInfo.CurrentCulture))
                        {
                            csvReader.Configuration.Delimiter = ",";
                            csvReader.WriteRecords(tableNames);
                        }
                    }

                }
            }
        }
    }

    public class TableNames
    {
        [Name("Ідентифікаційний код юридичної особи")]
        public string internalId     { get; set; }

        [Name("Контолюючий орган")]
        public string regulator { get; set; }

        [Name("Сфера контролю")]
        public string sphere { get; set; }

        [Name("Перевірка №")]
        public string resultActNumber { get; set; }
        
        [Name("Статус перевірки")]
        public string status { get; set; }
        
        [Name("Ступінь ризику")]
        public string risk { get; set; }
        
        [Name("Тип перевірки")]
        public string isPlanned { get; set; }
        
        [Name("Санкції  (грн.)")]
        public string sanction { get; set; }
        
        [Name("Дати проведення")]
        public string date { get; set; }
        
        [Name("Посилання на картку з результатами")]
        public string link { get; set; }
    }    
}
